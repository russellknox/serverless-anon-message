## Anonymous Commenting Mockup
Allow anonymous messages to be processed by different subscribers for differnet stakeholders 
#### HTTP endpoint
- /comment will take the event body and publish this to all SNS subscribers
```
{
    "title":"title of the comment", 
    "message":"content of the message"
}
```
#### Subscribers so far
- Dynamodb - puts event body in dynamodb. Custom stream events might be used for data processing if required
- Elastic - create/udpate elasticsearch index with event body. Data will be availble for visulisation in Kibana

#### To run this example locally, you'll need docker and serverless installed 
```
docker-compose up
sls offline start
```
Docker will start a local running instance of Dynamodb and Elastic and serverless will connect everything together
Use Postman or curl http://localhost:3000/comment
```
curl -d '{"title":"title of the comment", "message":"content of the message"}' -X POST http://localhost:3000/comment
```
#### TODO
- Cognito for anon auth
- Add subscribers according to brief
