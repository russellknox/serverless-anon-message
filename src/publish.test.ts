import { publish } from './publish'
import { APIGatewayEvent, Context, Callback } from 'aws-lambda'
import * as sns from './sns'

describe('publish', () => {
  it('should return 400 when no message param on body event', async () => {
    const event = { body: JSON.stringify({ title: 'A title' }) }
    const response = await publish(
      event as APIGatewayEvent,
      {} as Context,
      {} as Callback,
    )
    expect(response).toMatchSnapshot()
  })
  it('should return 400 when no title param on body event', async () => {
    const event = { body: JSON.stringify({ message: 'A message' }) }
    const response = await publish(
      event as APIGatewayEvent,
      {} as Context,
      {} as Callback,
    )
    expect(response).toMatchSnapshot()
  })
  it('should return 200 when sns publish resolves', async () => {
    const event = {
      body: JSON.stringify({ message: 'A message', title: 'A title' }),
    }
    const addToSns = jest.spyOn(sns, 'addToSns')
    addToSns.mockResolvedValue()
    const response = await publish(
      event as APIGatewayEvent,
      {} as Context,
      {} as Callback,
    )
    expect(response).toMatchSnapshot()
  })
  it('should return 400 when sns publish rejects', async () => {
    const event = {
      body: JSON.stringify({ message: 'A message', title: 'A title' }),
    }
    const addToSns = jest.spyOn(sns, 'addToSns')
    addToSns.mockRejectedValue(false)
    const response = await publish(
      event as APIGatewayEvent,
      {} as Context,
      {} as Callback,
    )
    expect(response).toMatchSnapshot()
  })
})
