import { Client, IndexDocumentParams } from 'elasticsearch'
import { SNSEvent } from 'aws-lambda'

const client: Client = new Client({
  host: process.env.ES_HOST,
  log: 'trace',
})

type comment = {
  title: string
  message: string
}

export const elastic = async (event: SNSEvent) => {
  const { title, message }: comment = JSON.parse(event.Records[0].Sns.Message)

  try {
    const params: IndexDocumentParams<comment> = await client.index({
      index: process.env.ES_INDEX,
      type: process.env.ES_TYPE,
      id: title,
      body: {
        title,
        message,
      },
    })
    return params
  } catch (error) {
    throw new Error(error)
  }
}
