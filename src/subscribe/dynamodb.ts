import { DynamoDB } from 'aws-sdk'
import * as shortid from 'shortid'
import { SNSEvent } from 'aws-lambda'

type comment = {
  title: string
  message: string
}

const isOffline = process.env.isOffline

const DB = new DynamoDB.DocumentClient({
  endpoint: isOffline ? 'http://localhost:8000' : 'http://localhost:8000',
  region: 'eu-west-2',
})

export const dynamodb = async (event: SNSEvent) => {
  const { title, message }: comment = JSON.parse(event.Records[0].Sns.Message)

  const Item = {
    Title: title,
    Message: message,
    Date: new Date().toString(),
    id: shortid.generate(),
  }

  const params = {
    Item,
    TableName: process.env.DB_TABLE,
  }

  try {
    const items = await DB.put(params).promise()
    return items
  } catch (error) {
    throw new Error(error)
  }
}
