import { SNS } from 'aws-sdk'

const sns = new SNS({
  endpoint: 'http://localhost:4002',
  region: 'eu-west-2',
})

const topicArn = 'arn:aws:sns:eu-west-2:123456789012:comment-topic'

export const addToSns = async ({ title, message, TopicArn = topicArn }) => {
  try {
    const params: SNS.PublishInput = {
      Message: JSON.stringify({ title, message }),
      TopicArn,
    }
    await sns.publish(params).promise()
  } catch (error) {
    throw new Error(error)
  }
}
