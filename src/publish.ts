import { APIGatewayProxyHandler, APIGatewayEvent } from 'aws-lambda'
import { addToSns } from './sns'

export const publish: APIGatewayProxyHandler = async (
  event: APIGatewayEvent,
) => {
  const { title, message } = JSON.parse(event.body)

  if (!title || !message) {
    return {
      statusCode: 400,
      body: JSON.stringify({ message: 'Must provide both title and message' }),
    }
  }
  try {
    await addToSns({ title, message })
    return {
      statusCode: 200,
      body: JSON.stringify({ message: 'Sucessfully published to queue' }),
    }
  } catch (error) {
    return {
      statusCode: 400,
      body: JSON.stringify('Error'),
    }
  }
}
